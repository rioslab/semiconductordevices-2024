# Reference for Semiconductor Devices

## Basic Knowledge

Tutorial: Coming soon!

## Books

Principles of Semiconductor Devices, Sima Dimitrijev, Oxford University Press, 2nd Edition, 2012.

Semiconductor Physics and Devices Basic Principles, Donald A. Neamen, McGraw-Hill Higher Education, 4th Edition, 2012.

## Papers

Coming soon!

