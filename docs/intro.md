# Syllabus

## Grading Policy

<div style="text-align:justify;text-justify:inter-ideograph">
  <table  bgcolor="#ffffff" style="border:1px solid black;border-collapse:collapse;" border="1" >
    <thead>
      <tr class="header">
        <th style="text-align: center;">Evaluation Type</th>
        <th style="text-align: center;">Overall Percent</th>
      </tr>
    </thead>
    <tbody>
      <tr class="odd">
          <td style="text-align: center;">Homework</td>
          <td style="text-align: center;">10%</td>
      </tr>
    <tr class="even">
    <td style="text-align: center;">In-Class Quiz</td>
    <td style="text-align: center;">10%</td>
    </tr>
    <tr class="odd">
    <td style="text-align: center;">Midterm Exam</td>
    <td style="text-align: center;">25%</td>
    </tr>
    <tr class="even">
    <td style="text-align: center;">Final Exam</td>
    <td style="text-align: center;">35%</td>
    </tr>
    <tr class="odd">
    <td style="text-align: center;">Final Project</td>
    <td style="text-align: center;">20%</td>
    </tr>
    </tbody>
    </table>

</div>


## Course Schedule

<div style="text-align:justify;text-justify:inter-ideograph">
<table  bgcolor="#ffffff" style="border:1px solid black;border-collapse:collapse;" border="1">
      <thead>
  <tr>
  <th style="text-align:center">Lecture</th>
  <th style="text-align:center">Date</th>
  <th style="text-align:center">Topic</th>
  <th style="text-align:center">Paper Reading</th>
  <th style="text-align:center">Reference Book Reading</th>
  <th style="text-align:center">Teaching Slides</th>
  </tr>
  </thead>
  <tbody>
  <tr>
  <td style="text-align:center">1</td>
  <td style="text-align:center">03/01/2024</td>
  <td style="text-align:center">Introduction</td>
  <td style="text-align:center"></td>
  <td style="text-align:center"></td>
  <td style="text-align:center">
  <p><a target="_blank" href="https://gitlab.com/rioslab/semiconductordevices-2024/-/blob/main/lectures/Lecture1.pdf?ref_type=heads"> Lecture 1</a></p>
  </td>
  </tr>

  <tr>
  <td style="text-align:center">2</td>
  <td style="text-align:center">03/08/2024</td>
  <td style="text-align:center">Classic Model of Conduction</td>
  <td style="text-align:center"></td>
  <td style="text-align:center">Chapter 3 for this lecture; Chapter 2.1, 2.2, 2.3 before next lecture</td>
  <td style="text-align:center">
  <p><a target="_blank" href="https://gitlab.com/rioslab/semiconductordevices-2024/-/blob/main/lectures/Lecture2.pdf?ref_type=heads"> Lecture 2</a></p>
  </td>
  </tr>

  <tr>
  <td style="text-align:center">3</td>
  <td style="text-align:center">03/15/2024</td>
  <td style="text-align:center">Quantum Mechanics, Electrons in Crystal</td>
  <td style="text-align:center"></td>
  <td style="text-align:center">Chapter 2.1, 2.2, 2.3 for this lecture; 2.4 before next lecture</td>
  <td style="text-align:center">
  <p><a target="_blank" href="https://gitlab.com/rioslab/semiconductordevices-2024/-/blob/main/lectures/Lecture3.pdf?ref_type=heads"> Lecture 3</a></p>
  </td>
  </tr>

  <tr>
  <td style="text-align:center">4</td>
  <td style="text-align:center">03/22/2024</td>
  <td style="text-align:center">Real Crystal, Intrinsic Semiconductor</td>
  <td style="text-align:center"></td>
  <td style="text-align:center">Chapter 1.1, 1.2.1, 2.4 for this lecture; 1.2.2, 1.2.3, 1.2.4 and entire Chapter 4 & Chapter 5 before next lecture</td>
  <td style="text-align:center">
  <p><a target="_blank" href="https://gitlab.com/rioslab/semiconductordevices-2024/-/blob/main/lectures/Lecture4.pdf?ref_type=heads"> Lecture 4</a></p>
  </td>
  </tr>

  <tr>
  <td style="text-align:center">5</td>
  <td style="text-align:center">03/29/2024</td>
  <td style="text-align:center">Doped Semiconductor, Drift, Diffusion, Recombination</td>
  <td style="text-align:center"></td>
  <td style="text-align:center">Chapter 1.2.2, 1.2.3, 1.2.4 and entire Chapter 4 & Chapter 5 for this lecture; Chapter 6.1 before next lecture</td>
  <td style="text-align:center">
  <p><a target="_blank" href="https://gitlab.com/rioslab/semiconductordevices-2024/-/blob/main/lectures/Lecture5.pdf?ref_type=heads"> Lecture 5</a></p>
  </td>
  </tr>

  <tr>
  <td style="text-align:center">6</td>
  <td style="text-align:center">04/12/2024</td>
  <td style="text-align:center">Static P-N Junctions</td>
  <td style="text-align:center"></td>
  <td style="text-align:center">Chapter 6.1 for this lecture; Chapter 6.2, 6.3 and 6.4 before next lecture</td>
  <td style="text-align:center">
  <p><a target="_blank" href="https://gitlab.com/rioslab/semiconductordevices-2024/-/blob/main/lectures/Lecture6.pdf?ref_type=heads"> Lecture 6</a></p>
  </td>
  </tr>

  <tr>
  <td style="text-align:center">7</td>
  <td style="text-align:center">04/19/2024</td>
  <td style="text-align:center">P-N Junctions with Applied Bias</td>
  <td style="text-align:center"></td>
  <td style="text-align:center">Chapter 6.2, 6.3 and 6.4 for this lecture; Chapter 12 and Chapter 9 before next lecture</td>
  <td style="text-align:center">
  <p><a target="_blank" href="https://gitlab.com/rioslab/semiconductordevices-2024/-/blob/main/lectures/Lecture7.pdf?ref_type=heads"> Lecture 7</a></p>
  </td>
  </tr>

  <tr>
  <td style="text-align:center">8</td>
  <td style="text-align:center">05/10/2024</td>
  <td style="text-align:center">Midterm Exam Solutions Review; Device Applications on PN Junction: LED, Laser, Solar Cell; BJT</td>
  <td style="text-align:center"></td>
  <td style="text-align:center">Chapter 12 and Chapter 9 for this lecture; Chapter 7.1 before next lecture</td>
  <td style="text-align:center">
  <p><a target="_blank" href="https://gitlab.com/rioslab/semiconductordevices-2024/-/blob/main/lectures/Lecture8.pdf?ref_type=heads"> Lecture 8</a></p>
  <p><a target="_blank" href="https://gitlab.com/rioslab/semiconductordevices-2024/-/blob/main/lectures/Lecture9.pdf?ref_type=heads"> Lecture 9</a></p>
  </td>
  </tr>

  <tr>
  <td style="text-align:center">9</td>
  <td style="text-align:center">05/17/2024</td>
  <td style="text-align:center">Metal-Semiconductor Junction</td>
  <td style="text-align:center"></td>
  <td style="text-align:center">Chapter 7.1 for this lecture; Chapter 7.2 before next lecture</td>
  <td style="text-align:center">
  <p><a target="_blank" href="https://gitlab.com/rioslab/semiconductordevices-2024/-/blob/main/lectures/Lecture10.pdf?ref_type=heads"> Lecture 10</a></p>
  </td>
  </tr>

  <tr>
  <td style="text-align:center">10</td>
  <td style="text-align:center">05/24/2024</td>
  <td style="text-align:center">MOS Capacitor</td>
  <td style="text-align:center"></td>
  <td style="text-align:center">Chapter 7.2 for this lecture; Chapter 8 before next lecture</td>
  <td style="text-align:center">
  <p><a target="_blank" href="https://gitlab.com/rioslab/semiconductordevices-2024/-/blob/main/lectures/Lecture11.pdf?ref_type=heads"> Lecture 11</a></p>
  </td>
  </tr>

  <tr>
  <td style="text-align:center">11</td>
  <td style="text-align:center">05/31/2024</td>
  <td style="text-align:center">MOS Transistors: Structure, I-V</td>
  <td style="text-align:center"></td>
  <td style="text-align:center">Chapter 8 for this lecture</td>
  <td style="text-align:center">
  <p><a target="_blank" href="https://gitlab.com/rioslab/semiconductordevices-2024/-/blob/main/lectures/FinalProject.pdf?ref_type=heads"> Final Project</a></p>
  </td>
  </tr>

  <tr>
  <td style="text-align:center">12</td>
  <td style="text-align:center">06/07/2024</td>
  <td style="text-align:center">MOS Transistors: Characterizations, Modifications, Circuits, CMOS, Channel Length Modulation, Band Diagrams</td>
  <td style="text-align:center"></td>
  <td style="text-align:center">Chapter 8 for this lecture</td>
  <td style="text-align:center">
  <p><a target="_blank" href="https://gitlab.com/rioslab/semiconductordevices-2024/-/blob/main/lectures/Lecture12.pdf?ref_type=heads"> Lecture 12</a></p>
  </td>
  </tr>

  <tr>
  <td style="text-align:center">13</td>
  <td style="text-align:center">06/14/2024</td>
  <td style="text-align:center">Leakage Currents for MOSFET</td>
  <td style="text-align:center"></td>
  <td style="text-align:center"></td>
  <td style="text-align:center">
  <p><a target="_blank" href="https://gitlab.com/rioslab/semiconductordevices-2024/-/blob/main/lectures/Lecture13.pdf?ref_type=heads"> Lecture 13</a></p>
  </td>
  </tr>

  </tbody>
  </table>
</div>
