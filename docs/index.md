# Welcome to Semiconductor Devices

This is the course website of Semiconductor Devices, instructed by Dr. Lei Ren.

## Semester/Year

Spring 2024

## Class Hours

01:30 - 03:45 PM (Friday)

## Classroom

C403B, SIGS International Phase 1 Block C, University Town of Shenzhen, Nanshan District, City of Shenzhen, Guangdong Province, China, 518055

## Course Description

This course is intended to provide students with both the basic and advanced knowledge on part of semiconductor physics and most of semiconductor devices, with an emphasis on those fundamental devices in nanoscale as the building blocks for our current AI and big data driven IT world such as diode, triode, transistors, MOSFET, CMOS, FinFET and their operation principles, to meet the major technological challenges in the contemporary environment. State of the art CMOS scaling technologies beyond 22nm and further physical difficulties for chips below 10nm will be included. This course aims for graduate students of computer or electrical engineering major with basic college physics and college mathematics as prerequisites from their undergraduate studies.

## Reference Book
<div style="text-align:justify;text-justify:inter-ideograph">
Principles of Semiconductor Devices, Sima Dimitrijev, Oxford University Press, 2nd Edition, 2012.
</div>
